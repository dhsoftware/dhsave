dhsave

DCAL Macro for DataCAD that saves the current drawing and then creates a date/time-stamped copy of the saved file.
Copy the dhSave.dcx file to your DataCAD Macro folder.  The downloaded zip file also contains icons that can be used to add the macro to a DataCAD toolbar.
